DEVICES=(Master Headphone Front Surround Center LFE)
CONFIGS=()
for DEV in "${DEVICES[@]}"; do
  PREV_STATE=$(amixer -c 1 get $DEV | tail -1 | cut -d':' -f2 | tr --squeeze-repeats " " | cut -d" " -f 4 | sed -nE "s/\[([^\)]*)\]/\1/p");
  PREV_VOL=$(amixer -c 1 get $DEV | tail -1 | cut -d':' -f2 | tr --squeeze-repeats " " | cut -d" " -f 6 | sed -nE "s/\[([^\)]*)\]/\1/p");
  echo "$DEV: $PREV_STATE $PREV_VOL";
done;
